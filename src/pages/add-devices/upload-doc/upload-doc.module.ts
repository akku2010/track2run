import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UploadDocPage } from './upload-doc';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    UploadDocPage,
  ],
  imports: [
    IonicPageModule.forChild(UploadDocPage),
    TranslateModule.forChild()
  ],
})
export class UploadDocPageModule {}
